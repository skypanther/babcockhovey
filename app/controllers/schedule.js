var constants = require('constants');

/*
	First, populate the daytime schedule table
*/
function pad(num, size) {
	var s = num + "";
	while (s.length < size) s = "0" + s;
	return s;
}
var now = new Date(),
	rowIDs = [],
	fridayTableRowIDs = [],
	nowstring = now.getHours() + '' + pad(now.getMinutes(), 2);

function populateTable(text) {
	var lines = (text).split(/\r\n|\r|\n/g),
		rows = [];
	for (var c = 1, l = lines.length; c < l; c++) {
		var line = lines[c];
		var rowData = line.split(",");
		rowIDs.push(parseInt(rowData[0]));
		if (rowData[1] != 'x') {
			args = {
				id: rowData[0],
				time: rowData[1],
				theevent: rowData[2],
				color: constants.styles.rowColor,
			}
			var r = Alloy.createController('scheduleRow', args).getView();
			rows.push(r);
		}
	}
	for (var i = 0, j = rows.length; i < j; i++) {
		try {
			if (parseInt(nowstring) >= parseInt(rows[i].id) && parseInt(nowstring) < rowIDs[i + 1]) {
				rows[i].backgroundColor = constants.styles.rowBGHighlight;
			}
		} catch (err) {}
	}
	$.scheduleTable.setData(rows);
}

function fillTable(f) {
	populateTable(f.fileText);
}
var cf = require('filecache');
cf.cachedFile({
	filename: constants.files.schedule,
	url: constants.files.scheduleURL,
	age: 1000 * 60 * 60 * 24,
	defaultFile: '/schedule.csv'
}, fillTable);

/*
	Now, populate the hidden evening schedule table
*/
function populateEveningTable(text) {
	var lines = (text).split(/\r\n|\r|\n/g),
		rows = [];
	for (var c = 1, l = lines.length; c < l; c++) {
		var line = lines[c];
		var rowData = line.split(",");
		args = {
			theevent: rowData[1],
			meritbadge: rowData[2],
			isHeading: (rowData[0]) ? true : false,
			color: constants.styles.rowColor,
		}
		var r = Alloy.createController('eveningRow', args).getView();
		rows.push(r);
	}
	$.eveningTable.setData(rows);
}

function fillEveningTable(f) {
	populateEveningTable(f.fileText);
}
var cf = require('filecache');
cf.cachedFile({
	filename: constants.files.evening,
	url: constants.files.eveningURL,
	age: 1000 * 60 * 60 * 24,
	defaultFile: '/eveningschedule.csv'
}, fillEveningTable);

/*
	Next, populate the Friday table
*/
function populateFridayTable(text) {
	var lines = (text).split(/\r\n|\r|\n/g),
		rows = [];
	for (var c = 1, l = lines.length; c < l; c++) {
		var line = lines[c];
		var rowData = line.split(",");
		fridayTableRowIDs.push(parseInt(rowData[0]));
		if (rowData[1] != 'x') {
			args = {
				id: rowData[0],
				time: rowData[1],
				theevent: rowData[2],
				color: constants.styles.rowColor,
			}
			var r = Alloy.createController('scheduleRow', args).getView();
			rows.push(r);
		}
	}
	for (var i = 0, j = rows.length; i < j; i++) {
		try {
			if (parseInt(nowstring) >= parseInt(rows[i].id) && parseInt(nowstring) < fridayTableRowIDs[i + 1]) {
				rows[i].backgroundColor = constants.styles.rowBGHighlight;
			}
		} catch (err) {}
	}
	$.fridayTable.setData(rows);
}

function fillFridayTable(f) {
	populateFridayTable(f.fileText);
}
var cf = require('filecache');
cf.cachedFile({
	filename: constants.files.fridaySchedule,
	url: constants.files.fridayScheduleURL,
	age: 1000 * 60 * 60 * 24,
	defaultFile: '/fridayschedule.csv'
}, fillFridayTable);
/*
	Now, handle the day/evening buttons
*/
$.bar.addEventListener('click', function (e) {
	switch (e.source.id) {
	case 'btnDaytime':
		$.scheduleTable.zIndex = 2;
		$.eveningTable.zIndex = 1;
		$.fridayTable.zIndex = 1;
		$.btnEvening.applyProperties({
			color: '#555',
			backgroundColor: '#5e7872',
			font: {
				fontWeight: 'bold'
			}
		});
		$.btnFriday.applyProperties({
			color: '#555',
			backgroundColor: '#5e7872',
			font: {
				fontWeight: 'bold'
			}
		});
		$.btnDaytime.applyProperties({
			color: 'white',
			backgroundColor: '#1D183F',
			font: {
				fontWeight: 'bold'
			}
		});
		break;
	case 'btnEvening':
		$.scheduleTable.zIndex = 1;
		$.eveningTable.zIndex = 2;
		$.fridayTable.zIndex = 1;
		$.btnDaytime.applyProperties({
			color: '#555',
			backgroundColor: '#5e7872',
			font: {
				fontWeight: 'bold'
			}
		});
		$.btnFriday.applyProperties({
			color: '#555',
			backgroundColor: '#5e7872',
			font: {
				fontWeight: 'bold'
			}
		});
		$.btnEvening.applyProperties({
			color: 'white',
			backgroundColor: '#1D183F',
			font: {
				fontWeight: 'bold'
			}
		});
		break;
	case 'btnFriday':
		$.scheduleTable.zIndex = 1;
		$.eveningTable.zIndex = 1;
		$.fridayTable.zIndex = 2;
		$.btnEvening.applyProperties({
			color: '#555',
			backgroundColor: '#5e7872',
			font: {
				fontWeight: 'bold'
			}
		});
		$.btnDaytime.applyProperties({
			color: '#555',
			backgroundColor: '#5e7872',
			font: {
				fontWeight: 'bold'
			}
		});
		$.btnFriday.applyProperties({
			color: 'white',
			backgroundColor: '#1D183F',
			font: {
				fontWeight: 'bold'
			}
		});
		break;
	}
});

function updateHighlight(tableSection, whichTable) {
	now = new Date();
	nowstring = now.getHours() + '' + pad(now.getMinutes(), 2);
	console.log('Setting time to ' + now.toLocaleString());

	var arrayOfIds = whichTable === 'friday' ? fridayTableRowIDs : rowIDs;

	for (var i = 0, j = tableSection.rows.length; i < j; i++) {
		tableSection.rows[i].backgroundColor = 'transparent';
		try {
			if (parseInt(nowstring) >= parseInt(tableSection.rows[i].id) && parseInt(nowstring) < arrayOfIds[i + 1]) {
				tableSection.rows[i].backgroundColor = constants.styles.rowBGHighlight;
			}
		} catch (err) {}
	}

}

exports.updateTime = function () {
	updateHighlight($.scheduleTable.sections[0]);
	updateHighlight($.fridayTable.sections[0], 'friday');
};