// populates the ui table
var challenges = [];
var populateTable = function () {
	Alloy.Globals.loading.show('Loading ...', false);
	challenges = [];
	var dbConn = Ti.Database.open('challenges'),
		rows = dbConn.execute('SELECT * FROM challenges ORDER BY done DESC, id ASC'),
		data = [];
	while (rows.isValidRow()) {
		challenges.push({
			id: rows.fieldByName('id'),
			chsecret: rows.fieldByName('chsecret'),
		});
		var row = Ti.UI.createTableViewRow({
			id: rows.fieldByName('id'),
			chsecret: rows.fieldByName('chsecret'),
			title: '',
			height: 60
		});
		/*		row.add(Ti.UI.createLabel({
			id: 'checkbox',
			height: 20, width: 20,
			borderRadius: 2,
			left: 20, top: 5,
			font: {
				fontWeight: 'bold',
				fontSize: 18
			},
			borderColor: '#302703',
			borderWidth: 1,
			color: '#302703',
			text: (rows.fieldByName('done')) ? '\u2713' : ' ',
			textAlign: 'center'
		}));
*/
		var v = Ti.UI.createView({
			width: Ti.UI.FILL,
			height: Ti.UI.SIZE,
			layout: 'vertical'
		})
		v.add(Ti.UI.createLabel({
			color: '#302703',
			left: 10,
			top: 2,
			text: rows.fieldByName('chtext'),
			font: {
				fontSize: 15,
				fontWeight: (rows.fieldByName('required')) ? 'bold' : 'normal'
			}
		}));
		v.add(Ti.UI.createLabel({
			color: '#302703',
			left: 10,
			top: 2,
			text: "Sign off: " + rows.fieldByName('approvedby'),
			font: {
				fontSize: 13
			}
		}));
		row.add(v);
		data.push(row);
		rows.next();
	}
	data.push(Ti.UI.createTableViewRow({
		id: 9999999999999,
		chsecret: '',
		title: '     ',
		height: 30
	}));
	$.tblChallenges.setData(data);
	Alloy.Globals.loading.hide();
};

// populates the database
var populateCollection = function (f) {
	if (f) {
		Alloy.Globals.loading.show('Loading ...', false);
		var dbConn = Ti.Database.open('challenges');
		//dbConn.execute('BEGIN TRANSACTION');
		dbConn.execute('DROP TABLE IF EXISTS challenges');
		dbConn.execute('CREATE TABLE IF NOT EXISTS challenges (id INTEGER PRIMARY KEY, chtext TEXT, required TEXT, approvedby TEXT, chsecret TEXT, done INTEGER)');
		var lines = (f.fileText).split(/\r\n|\r|\n/g),
			rows = [];
		for (var c = 1, l = lines.length; c < l; c++) {
			var line = lines[c],
				rowData = line.split(",");
			dbConn.execute('INSERT INTO challenges (id, chtext, required, approvedby, chsecret, done) VALUES (?, ?, ?, ?, ?, 0)', c - 1, rowData[1], rowData[2], rowData[3], rowData[4]);
		}
		//dbConn.execute('COMMIT');
		dbConn.close();
		populateTable();
		Alloy.Globals.loading.hide();
	}
};

if (!Ti.App.Properties.hasProperty('challengesseeded')) {
	var cf = require('filecache'),
		constants = require('constants');
	cf.cachedFile({
		filename: constants.files.challenges,
		url: constants.files.challengesURL,
		age: 1000 * 60 * 60 * 24,
		defaultFile: '/challenges.csv'
	}, populateCollection);
	Ti.App.Properties.setInt('challengesseeded', 1);
} else {
	populateTable();
}

// function for determining if the challenge was met and marking the
// row in the database
var markDB = function (secret) {
	for (var i = 0, j = challenges.length; i < j; i++) {
		if (secret = challenges[i].chsecret) {
			var dbConn = Ti.Database.open('challenges');
			dbConn.execute('UPDATE challenges SET done = 1 WHERE id = ?', challenges[i].id);
			populateTable();
		} else {
			alert('Not a valid barcode');
		}
	}
}

/*$.checkinwrapper.addEventListener('click', function() {
	if(Ti.Media.isCameraSupported) {
		$.checkintext.text = 'wait...';
		setTimeout(function() {
			$.checkintext.text = 'Check in';
		}, 250);
		// open barcode scanner
		var Barcode = require('ti.barcode');
		Barcode.allowRotation = true;
		Barcode.displayedMessage = 'Scan to check in';
		Barcode.allowMenu = false;
		Barcode.allowInstructions = false;
		Barcode.useLED = false;
	    Barcode.capture({
	        animate: true,
	        showCancel: true,
	        showRectangle: true,
	        keepOpen: false,
	        acceptedFormats: [
	            Barcode.FORMAT_QR_CODE
	        ]
	    });
		Barcode.addEventListener('success', function (e) {
		    Ti.API.info('Success called with barcode: ' + e.result);
		    markDB(e.result);
		});
	} else {
		alert('Sorry, you need a camera to scan the QR code and check in.')
	}
});
*/