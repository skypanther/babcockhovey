var constants = require('constants');
function doCall(e) {
	$.phonetext.color = constants.styles.textOn;
	$.phoneicon.color = constants.styles.textOn;
	setTimeout(function(){
		$.phonetext.color = constants.styles.textOff;
		$.phoneicon.color = constants.styles.textOff;
	}, 125);
	Ti.Platform.openURL('tel://607-869-3841');
}
/*
$.phonetext.addEventListener('click', doCall);
$.phoneicon.addEventListener('click', doCall);
*/
function doNavigate(e) {  
	$.addressicon.color = constants.styles.textOn;
	$.addresstext.color = constants.styles.textOn;
	setTimeout(function(){
		$.addressicon.color = constants.styles.textOff;
		$.addresstext.color = constants.styles.textOff;
	}, 125);
	if(OS_IOS) {
	    Ti.Platform.openURL('maps://maps.google.com/maps?daddr=7294+County+Road+132+Ovid,+NY+14521');
	} else {
		// old values: 42.661813,-76.8710695
		Ti.Platform.openURL('http://maps.google.com/maps?daddr=42.6693833,-76.8567666&directionsmode=driving');
	}
}
$.addressicon.addEventListener('click', doNavigate);
$.addresstext.addEventListener('click', doNavigate);

// handle picture of the day
var largePicOfTheDay;
function setImage(obj) {
	$.todaypic.image = constants.resizeImage({
		file: obj.nativePath,
		height: 300
		});
	largePicOfTheDay = obj.fileBlob;
}
var cf = require('filecache');
cf.cachedFile({
	filename: constants.files.picoday,
	url: constants.files.picodayURL,
	defaultFile: 'files/pictureoftheday/today.jpg'
}, setImage);
function showBigPic() {
	var win = Alloy.createController('picoftheday', {picture: largePicOfTheDay}).getView();
	win.open({modal: true, modalTransitionStyle: Titanium.UI.iPhone.MODAL_TRANSITION_STYLE_CROSS_DISSOLVE});
}
$.todaypic.addEventListener('click', showBigPic);
$.zoomin.addEventListener('click', showBigPic);

// add the button click handlers
var doSwitchPage = function(e) {
	var constants = require('constants');
	e.source.color = constants.styles.textOff;
	setTimeout(function(){
		e.source.color = constants.styles.textOn;
	}, 125);
	Alloy.Globals.switchPage((e.source.id).replace('icon',''));
};