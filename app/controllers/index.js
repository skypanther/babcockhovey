var NOTOGGLE = true;

function toggle(e) {
	var fn = 'toggle' + e.source.win + 'Window';
	if (fn === 'toggleRightWindow') {
		$.scheduleWindow.updateTime();
	}
	$.drawer[fn]();
}
var newContent;

function switchPage(id, notoggle) {
	switch (id) {
	case 'home':
		newContent = Alloy.createController('home');
		$.content.removeAllChildren();
		$.content.add(newContent.getView());
		if (OS_IOS) $.contentWindow.title = "Babcock-Hovey";
		if (!notoggle) $.drawer["toggleLeftWindow"]();
		break;
	case 'scoutmasters':
		newContent = Alloy.createController('scoutmasters');
		$.content.removeAllChildren();
		$.content.add(newContent.getView());
		if (OS_IOS) $.contentWindow.title = "Scoutmasters";
		if (!notoggle) $.drawer["toggleLeftWindow"]();
		break;
	case 'news':
		if (OS_IOS) {
			newContent = Alloy.createController('news');
			$.content.removeAllChildren();
			$.content.add(newContent.getView());
			$.contentWindow.title = "News";
			if (!notoggle) $.drawer["toggleLeftWindow"]();
		} else {
			if (!notoggle) $.drawer["toggleLeftWindow"]();
			var fireIntent = function (f) {
				var tempFile = Ti.Filesystem.getFile(Ti.Filesystem.tempDirectory, 'Camp_News.pdf');
				tempFile.write(f.fileBlob);
				var intent = Ti.Android.createIntent({
					action: Ti.Android.ACTION_VIEW,
					type: 'application/pdf',
					data: tempFile.nativePath
				});
				try {
					Ti.Android.currentActivity.startActivity(intent);
				} catch (ex) {
					/* Handle Exception if no suitable apps installed */
					Ti.UI.createNotification({
						message: 'No PDF viewer installed'
					}).show();
				}
			};
			var constants = require('constants');
			var cf = require('filecache');
			cf.cachedFile({
				filename: constants.files.news,
				url: constants.files.newsURL,
				age: 1000 * 60 * 60 * 24,
				defaultFile: '/news.pdf'
			}, fireIntent);
		}
		break;
	case 'badges':
		newContent = Alloy.createController('badges');
		$.content.removeAllChildren();
		$.content.add(newContent.getView());
		if (OS_IOS) $.contentWindow.title = "Badges";
		if (!notoggle) $.drawer["toggleLeftWindow"]();
		break;
	case 'staff':
		newContent = Alloy.createController('staff');
		$.content.removeAllChildren();
		$.content.add(newContent.getView());
		if (OS_IOS) $.contentWindow.title = "Staff";
		if (!notoggle) $.drawer["toggleLeftWindow"]();
		break;
	case 'about':
		newContent = Alloy.createController('about');
		$.content.removeAllChildren();
		$.content.add(newContent.getView());
		if (OS_IOS) $.contentWindow.title = "About";
		if (!notoggle) $.drawer["toggleLeftWindow"]();
		break;
	case 'map':
		var mapwin = Alloy.createController('map').getView();
		mapwin.open({
			modal: true
		});
		if (!notoggle) $.drawer["toggleLeftWindow"]();
		break;
	case 'schedule':
		$.scheduleWindow.updateTime();
		if (notoggle) $.drawer["toggleRightWindow"]();
		break;
	case 'schedule_menu':
		$.drawer["toggleLeftWindow"]();
		setTimeout(function () {
			$.scheduleWindow.updateTime();
			$.drawer["toggleRightWindow"]();
		}, 250);
		break;
	case 'refresh':
		expireCache();
		if (!notoggle) $.drawer["toggleLeftWindow"]();
		break;
	}
}

// Add menu event listeners
function doMenuSwitch(e) {
	if (e.source.id != 'menuwrapper') {
		switchPage(e.source.id);
	}
}
if (OS_ANDROID) {
	$.menuwrapper.addEventListener('click', doMenuSwitch);
} else {
	if (Ti.Platform.osname != 'ipod') {
		$.menuwrapper.addEventListener('click', doMenuSwitch);
	} else {
		$.icon_home.addEventListener('click', doMenuSwitch);
		$.home.addEventListener('click', doMenuSwitch);
		$.icon_schedule_menu.addEventListener('click', doMenuSwitch);
		$.schedule_menu.addEventListener('click', doMenuSwitch);
		$.icon_map.addEventListener('click', doMenuSwitch);
		$.map.addEventListener('click', doMenuSwitch);
		$.icon_scoutmasters.addEventListener('click', doMenuSwitch);
		$.scoutmasters.addEventListener('click', doMenuSwitch);
		$.icon_news.addEventListener('click', doMenuSwitch);
		$.news.addEventListener('click', doMenuSwitch);
		$.icon_badges.addEventListener('click', doMenuSwitch);
		$.badges.addEventListener('click', doMenuSwitch);
		$.icon_staff.addEventListener('click', doMenuSwitch);
		$.staff.addEventListener('click', doMenuSwitch);
		$.icon_about.addEventListener('click', doMenuSwitch);
		$.about.addEventListener('click', doMenuSwitch);
		$.refresh.addEventListener('click', doMenuSwitch);
	}
}

Alloy.Globals.switchPage = function (id) {
	switchPage(id, NOTOGGLE);
};

if (!Ti.App.Properties.getBool('toasted', false)) {
	if (OS_ANDROID) {
		var toast = Ti.UI.createNotification({
			message: "\u2194 Slide left/right for options",
			duration: Ti.UI.NOTIFICATION_DURATION_LONG
		});
		setTimeout(function () {
			toast.show();
		}, 250);
	} else {
		setTimeout(function () {
			$.drawer.bounceLeftWindow();
		}, 500);
	}
	Ti.App.Properties.setBool('toasted', true);
}
if (OS_ANDROID) {
	$.drawer.addEventListener('open', function (e) {
		var activity = e.source.getActivity();
		if (activity.actionBar) {
			activity.actionBar.displayHomeAsUp = true;
			activity.actionBar.onHomeIconItemSelected = $.drawer.toggleLeftWindow;
		}
	});
}
$.drawer.open();

function expireCache() {
	_.each(Ti.App.Properties.listProperties(), function (prop) {
		Ti.API.info('prop: ' + JSON.stringify(prop));
	});
}