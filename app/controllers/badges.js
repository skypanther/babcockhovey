var db = Ti.Database.install('/babcock2015.sqlite', 'babcock2015'),
	rows = db.execute('SELECT * FROM badges ORDER BY badge'),
	data = [];
while (rows.isValidRow()) {
	var arg = {
		badgeid: rows.fieldByName('id'),
		title: rows.fieldByName('badge'),
		key: rows.fieldByName('key')
	};
	var row = Alloy.createController('badgeRow', arg).getView();
	data.push(row);
	rows.next();
}
$.table.data = data;
$.table.bubbleParent = false;
db.close();

$.table.addEventListener('click', function (e) {
	// get the row's key e.rowData.key
	// look up in the db to find the x/y coords for that key
	var dbconn = Ti.Database.open('babcock2015');
	var spots = dbconn.execute('SELECT x, y, location FROM latlong WHERE key = ?', e.row.key),
		spot = [];
	while (spots.isValidRow()) {
		spot.push({
			x: spots.getFieldByName('x'),
			y: spots.getFieldByName('y'),
			loc: spots.getFieldByName('location')
		});
		spots.next();
	}
	dbconn.close();
	// open the map modal
	var mapwin = Alloy.createController('map', spot).getView();
	mapwin.open({
		modal: true
	});
	/*
	if(OS_ANDROID) {
		mapwin.addEventListener('load', function() {
			alert(e.row.title + ' is at the \n' + spot[0].loc);
			//Ti.App.fireEvent('appshowIcon', {top: parseInt(spot[0].x)/Ti.Platform.displayCaps.logicalDensityFactor, left: parseInt(spot[0].y)/Ti.Platform.displayCaps.logicalDensityFactor})
		});
	}
*/
});