/*
$.mugshot.image = args.mugshot;
$.campname.text = args.campname;
$.realname.text = args.realname;
$.phone.text = args.phone;
	
*/

Alloy.Globals.loading.show('Loading ...', false);

var constants = require('constants');
var cf = require('filecache');

function populateTable(text) {
	var lines = (text).split(/\r\n|\r|\n/g),
		rows = [];
	for (var c = 1, l = lines.length; c < l; c++) {
		var line = lines[c];
		var rowData = line.split(",");
		var args = {
			position: rowData[0],
			campname: rowData[1],
			realname: rowData[2],
			phone: rowData[3],
			mugshot: (rowData[4]) ? constants.files.staffPicURL + rowData[4] : '/staffplaceholder.png',
			photo: rowData[5] ? "https://drive.google.com/uc?export=download&id=" + rowData[5] : '/staffplaceholder.png'
		};
		var r = Alloy.createController('staffRow', args).getView();
		rows.push(r);
		console.log("https://drive.google.com/uc?export=download&id=" + rowData[5]);
	}
	$.staffTable.setData(rows);
	Alloy.Globals.loading.hide();
}

function fillTable(f) {
	if (f) populateTable(f.fileText);
}
cf.cachedFile({
	filename: constants.files.staff,
	url: constants.files.staffURL,
	age: 1000 * 60 * 60 * 24 * 10,
	defaultFile: '/staff.csv'
}, fillTable);

$.staffTable.addEventListener('click', function (e) {
	if (e.rowData.phone) {
		Ti.Platform.openURL('tel://' + e.rowData.phone);
	} else if (e.row.phone) {
		Ti.Platform.openURL('tel://' + e.row.phone);
	}
});