Alloy.Globals.loading.show('Loading ...', false);

var constants = require('constants');
var cf = require('filecache');
cf.cachedFile({
	filename: constants.files.news,
	url: constants.files.newsURL,
	age: 1000 * 60 * 60 * 24,
	defaultFile: '/files/news.pdf'
}, fillWebView);

function fillWebView(f) {
	if (OS_IOS) {
		$.news.data = f.fileBlob;
	} else {

	}
	Alloy.Globals.loading.hide();
}