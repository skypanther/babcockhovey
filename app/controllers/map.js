var args = arguments[0] || [];

if (OS_IOS) {
	setTimeout(function () {
		$.helpText.animate({
			opacity: 0,
			duration: 500
		}, function () {
			$.mapwindow.remove($.helpText);
		});
	}, 1500);
	$.mapwindow.addEventListener('doubletap', function () {
		$.mapwindow.close();
	});
} else {
	$.androidScrollView.scrollTo(0, 0);
}
if (args.length > 0) {
	if (OS_ANDROID) {
		var densityFactor = 2,
			xTweak = 1.03,
			yTweak = 1.08;
		if (Ti.Platform.displayCaps.logicalDensityFactor == 2) {
			xTweak = 1.05;
			yTweak = 1.09;
		} else if (Ti.Platform.displayCaps.logicalDensityFactor > 2) {
			densityFactor = 1.9;
			xTweak = 1.05;
			yTweak = 1.08;
		} else if (Ti.Platform.displayCaps.logicalDensityFactor == 1) {
			densityFactor = 1;
			xTweak = 1.1;
			yTweak = 1.12;
		}
		args[0].x = Math.ceil((args[0].x) / densityFactor * xTweak);
		args[0].y = Math.ceil((args[0].y) / densityFactor * yTweak);
	}
	$.mapwindow.addEventListener('open', function () {
		var img = Ti.UI.createImageView({
			image: '/images/location.png',
			center: {
				x: args[0].x,
				y: args[0].y
			},
			height: 100,
			width: 100,
			zIndex: 10
		});
		if (OS_IOS) {
			var matrix = Ti.UI.create2DMatrix().scale(.5, .5),
				a = Ti.UI.createAnimation({
					autoreverse: true,
					duration: 1000,
					transform: matrix,
					repeat: 100
				});
			img.animate(a);
			$.inner.add(img);
		} else {
			$.androidScrollView.add(img);
		}
	});
}