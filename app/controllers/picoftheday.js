var args = arguments[0] || {};

$.pic.image = args.picture;
$.pic.center = { x: '50%', y: '50%'};

if(OS_IOS) {
	setTimeout(function() {
		try {
			$.helpText.animate({
				opacity: 0,
				duration: 750
			});
		} catch (any) {};
	}, 1500);
	$.pic.addEventListener('doubletap', function() {
		$.picwindow.close();
	});
}
