var args = arguments[0] || {};

$.theevent.text = args.theevent;

if(args.meritbadge) {
	$.theevent.text = '     ' + args.meritbadge;
}

if(args.isHeading) {
	$.theevent.font = {
		fontWeight: 'bold',
		fontSize: 16
	}
	$.row.height = 45;
}

