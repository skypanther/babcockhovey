Alloy.Globals.winTop = (OS_IOS && parseInt(Ti.Platform.version, 10) >= 7) ? 20 : 0;
Alloy.Globals.homeTop = (OS_IOS && parseInt(Ti.Platform.version, 10) < 7) ? "-20" : 20;
if (OS_IOS && Ti.Platform.displayCaps.platformHeight < 568) {
	// short iOS device
	Alloy.Globals.homeTop = -20;
}
Ti.UI.backgroundColor = "#FFEDB7";

Alloy.Globals.actualHeight = Math.floor((Ti.Platform.displayCaps.platformHeight / Ti.Platform.displayCaps.logicalDensityFactor) * 1.01);
Alloy.Globals.scale = Alloy.Globals.actualHeight / 1125;
Alloy.Globals.startingWidth = 1500 * Alloy.Globals.scale;

/*
// database update code
switch (Ti.App.Properties.getInt('dbversion', 1)) {
case '1':
	// update database to move Footsteps to NAC
	Ti.API.debug('Updating database ...');
	var db = Ti.Database.install('/babcock.sqlite', 'babcock');
	db.execute('UPDATE badges SET key = "nac" WHERE id = 18');
	db.close();
	Ti.App.Properties.setInt('dbversion', 2);
	break;
default:
	break;
}
*/
Ti.App.Properties.setInt('dbversion', 2015); // replaced babcock.sqlite with babcock2015.sqlite on 13-July-2015

Alloy.Globals.loading = Alloy.createWidget("nl.fokkezb.loading");