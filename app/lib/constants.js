exports.styles = {
	textOn: 'white',
	textOff: '#163332',
	rowColor: '#163332',
	rowBGHighlight: '#f1c40f',
	darkText: '#302703'
};
exports.files = {
	schedule: 'schedule.csv',
	scheduleURL: 'https://docs.google.com/spreadsheets/d/15BK_cW21-KlbzTyPMumeM5p6ed-W80clOm6Q1Wab_5k/pub?gid=1477471689&single=true&output=csv',
	fridaySchedule: 'fridayschedule.csv',
	fridayScheduleURL: 'https://docs.google.com/spreadsheets/d/1_tRk8quhI5xwttzWW3Q2Vzss95hD8eSE4t3MK5OzIyA/pub?gid=1709301410&single=true&output=csv',
	picoday: 'today.jpg',
	picodayURL: 'https://dl.dropbox.com/s/o9lpcibtzefl3o5/today.jpg',
	staff: 'staff.csv',
	staffURL: 'https://docs.google.com/spreadsheets/d/1rCV9YSrRZULrBorOV18Rteu8sq9XO5yNlqStdLLkois/pub?gid=1728623010&single=true&output=csv',
	staffPicURL: '/files/staffphotos/',
	challenges: 'challenges.csv',
	challengesURL: 'https://docs.google.com/spreadsheets/d/1HonPZmKPyVf9T1gJ6K8hokIN2saDdaNb_gudInMqoLY/pub?gid=1920936392&single=true&output=csv',
	evening: 'eveningprograms',
	eveningURL: 'https://docs.google.com/spreadsheets/d/1DlTBdrX_t_t2M3we9aggi15hGTMouC2hNB__3roj95Q/pub?gid=859940512&single=true&output=csv',
	news: 'news.pdf',
	newsURL: 'https://dl.dropbox.com/s/0lrwyntq8ulwyp9/news.pdf'
};
exports.values = {
	titleBarHeight: 44
};


exports.resizeImage = function (obj) {
	var f = Ti.Filesystem.getFile(obj.file),
		blob = f.read(),
		rect = {
			width: blob.width,
			height: blob.height
		},
		thumb = {
			height: obj.height,
			width: (obj.height / rect.height) * rect.width
		};
	f = null;
	if (rect.height > rect.width) {
		thumb = {
			width: obj.height,
			height: (obj.height / rect.width) * rect.height
		};
	}

	if (OS_IOS) {
		var dimension = thumb.height;
	} else {
		var dimension = thumb.height / Ti.Platform.displayCaps.logicalDensityFactor;
	}
	blob = blob.imageAsResized(thumb.width, thumb.height);
	return blob.imageAsCropped({
		height: dimension,
		width: dimension
	});
};

/*
 * example usage
btnThumb.addEventListener('click', function (e) {
    var img = Ti.UI.createImageView({
        height: Ti.UI.SIZE,
        image: mod.resizeImage({
            file: Ti.Filesystem.getResourcesDirectory() + 'color.jpg',
            width: 120
        }),
        width: Ti.UI.SIZE
    });    
    win2.add(img);
});
*/