exports.rotateIn = function(view, _speed) {
	var speed = _speed || 300,
		t = Ti.UI.create2DMatrix();
    t = t.rotate(0);
    
    var a = Titanium.UI.createAnimation({
    	curve : Ti.UI.ANIMATION_CURVE_EASE_IN,
    	opacity: 0.85
    });
    a.transform = t;
    a.duration = speed;
	// set new anchorPoint on animation for Android
	a.anchorPoint = {x:0, y:0};
	// set new anchorPoint on view for iOS
	view.anchorPoint = {x:0, y:0};
    
    view.animate(a);
   };

exports.rotateOut = function(view, _speed) {
	var speed = _speed || 400,
		t = Ti.UI.create2DMatrix();
    t = t.rotate(90);
    
    var a = Titanium.UI.createAnimation({
    	curve : Ti.UI.ANIMATION_CURVE_EASE_OUT,
    	opacity: 0
    });
    a.transform = t;
    a.duration = speed;
	// set new anchorPoint on animation for Android
	a.anchorPoint = {x:0, y:0};
	// set new anchorPoint on view for iOS
	view.anchorPoint = {x:0, y:0};
    
    view.animate(a);
};
