var _getExtension = function(filename) {
	// from http://stackoverflow.com/a/680982/292947
	var re = /(?:\.([^.]+))?$/;
	var tmpext = re.exec(filename)[1];
	return (tmpext) ? tmpext : '';
};

/*
 * 
 * File caching pseudo code:
	arguments:
		f = {
			filename: 'foo.bar',
			url: 'http://example.com/filename.foo'
			defaultFile: '/files/filename.foo'
		},
		cb = function() 
*/
exports.cachedFile = function(f, cb) {
	var md5,
		needsToSave = false,
		savedFile,
		downloadDate,
		now = new Date(),
		d = now.getDate(),
		today = now.getFullYear() + '' + (now.getMonth()+1) + '' + ((d<10) ? '0'+d : d);
		file = {};
	f = f || {};
	if(f.filename){
		downloadDate = Ti.App.Properties.getString(f.filename, '20000101');
		md5 = Ti.Utils.md5HexDigest(f.filename)+"."+_getExtension(f.filename);
		savedFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,md5);
		if(savedFile.exists() &&  downloadDate == today){
			file.fileBlob = savedFile.read();
			file.fileText = (file.fileBlob).text;
			file.nativePath = savedFile.nativePath;
			cb(file);
		} else {
			if(!Ti.Network.online) {
				if(f.defaultFile) {
					var defaultFile = Titanium.Filesystem.getFile(Titanium.Filesystem.resourcesDirectory, '/files' + f.defaultFile),
						retFile = {};
					if(defaultFile.exists()) {
						retFile.fileBlob = defaultFile.read();
						retFile.fileText = (retFile.fileBlob).text;
						retFile.nativePath = defaultFile.nativePath;
						cb(retFile);
					} else {
						Ti.API.info('Default file not found');
					}
				} else {
					cb(false);
				}
			}
			var xhr = Ti.Network.createHTTPClient({
			    onload: function(e) {
					file.fileBlob = this.responseData;
					file.fileText = this.responseText;
					savedFile.write(this.responseData);
					file.nativePath = savedFile.nativePath;
					Ti.App.Properties.setString(f.filename, today);
					cb(file);
			    },
			    onerror: function(e) {
			        Ti.API.info(e.error);
					if(f.defaultFile) {
						var defaultFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, '/files' + f.defaultFile),
							retFile = {};
						if(defaultFile.exists()) {
							retFile.fileBlob = defaultFile.read();
							retFile.fileText = (retFile.fileBlob).text;
							retFile.nativePath = defaultFile.nativePath;
							cb(retFile);
						} else {
							Ti.API.info('Default file not found');
						}
					} else {
						throw 'Net error: no default file provided';
					}
			    },
			    timeout:10000  /* in milliseconds */
			});
			xhr.open("GET", f.url);
			xhr.send();  // request is actually sent with this statement

		}
	} else {
		throw 'Error: invalid arguments passed to cachedFile()';
	}
};
